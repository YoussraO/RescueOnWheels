import UIKit
import SpriteKit
import WebKit
import SwiftSocket

class GameViewController: UIViewController, WKUIDelegate {
    @IBOutlet weak var verticalSlider: UISlider!
    var minimumTrackTintColor = UIColor(red:0.36, green:0.30, blue:0.56, alpha:1.0)
    
    @objc func buttonAction(_ sender: UIButton) {
            GameScene().createConnection()
    }
    
    func presentConnectionAlert(){
        // create the alert
        let alert = UIAlertController(title: "Connection", message: "Het opzetten van de verbinding met de ROWPI1 is mislukt", preferredStyle: UIAlertControllerStyle.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Andere host", style: UIAlertActionStyle.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Stop verbinding", style: UIAlertActionStyle.cancel, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let btnsOffset: CGFloat = 10
        
        let scene = GameScene(size: self.view.bounds.size)
        
        if let skView = self.view as? SKView {
            let myWebView = WKWebView(frame: CGRect(x: 120, y:0, width:  view.frame.width - 120, height: (view.frame.height)))
            myWebView.isOpaque = false
            myWebView.backgroundColor = UIColor.clear
            myWebView.scrollView.backgroundColor = UIColor.clear

            if let myURL = URL(string: "http://rowpi1.ddns.net:8080/stream/video.mjpeg"){
                let myRequest = URLRequest(url: myURL)
                myWebView.load(myRequest)
            }
            skView.addSubview(myWebView)
            skView.ignoresSiblingOrder = true
            skView.presentScene(scene)
        }
        
        let connectBtn = UIButton(frame: CGRect(x: btnsOffset, y: 40, width: 90, height: 50))
        connectBtn.backgroundColor = .orange
        connectBtn.setTitle("Connect", for: .normal)
        connectBtn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        self.view.addSubview(connectBtn)
        
//        let client = TCPClient(address: "192.168.178.95", port: 8888)
//        switch client.connect(timeout: 1) {
//        case .success:
//            guard let data = client.read(1024*10) else { return }
//
//            if let response = String(bytes: data, encoding: .utf8) {
//                print(response)
//                client.send(string: "GET / HTTP/1.0\n\n" )
//            }
//        case .failure(let error):
//            print(error)
//        }
    }
    
    override var shouldAutorotate : Bool {
        return true
    }
    
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask  {
        return UIDevice.current.userInterfaceIdiom == .phone ? .allButUpsideDown : .all
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
}
