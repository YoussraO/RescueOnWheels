// Compile with $ gcc tcpserver.c -o tcpserver
// run with sudo ./tcpserver [portnumber]
// choice your own portnumber. and the tcp server starts running.
// todo: display the received message

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#define SOCKET_ERROR   ((int)-1)
#define SIZEBUF 1000000

void usage(void)
{  printf ("usage: servTCP LOCAL_PORT_NUMBER\n"); exit(1); }

int main(int argc, char *argv[])
{
#define MAXSIZE 1000000
    struct sockaddr_in Local, Cli;
    char string_remote_ip_address[100];
    short int remote_port_number, local_port_number;
    int socketfd, newsocketfd, OptVal, len, ris;
    int i,  n, nread, nwrite, recv_len;
    char buf[MAXSIZE];
    
    if(argc!=2) { printf ("1 parameter is needed\n"); usage(); exit(1);  }
    else {
        local_port_number = atoi(argv[1]);
    }
    
    printf ("socket()\n");
    socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd == SOCKET_ERROR) {
        printf ("socket() failed, Err: %d \"%s\"\n", errno,strerror(errno));
        exit(1);
    }
    
    /* avoid EADDRINUSE error on bind() */
    OptVal = 1;
    printf ("setsockopt()\n");
    ris = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, (char *)&OptVal, sizeof(OptVal));
    if (ris == SOCKET_ERROR)  {
        printf ("setsockopt() SO_REUSEADDR failed, Err: %d \"%s\"\n", errno,strerror(errno));
        exit(1);
    }
    
    /* name the socket */
    memset ( &Local, 0, sizeof(Local) );
    Local.sin_family        =    AF_INET;
    Local.sin_addr.s_addr    =htonl(INADDR_ANY);
    Local.sin_port        =    htons(local_port_number);
    printf ("bind()\n");
    ris = bind(socketfd, (struct sockaddr*) &Local, sizeof(Local));
    if (ris == SOCKET_ERROR)  {
        printf ("bind() failed, Err: %d \"%s\"\n",errno,strerror(errno));
        exit(1);
    }
    
    printf ("listen()\n");
    ris = listen(socketfd, 10 );
    if (ris == SOCKET_ERROR)  {
        printf ("listen() failed, Err: %d \"%s\"\n",errno,strerror(errno));
        exit(1);
    }
    
    /* wait for connection request */
    memset ( &Cli, 0, sizeof(Cli) );
    len=sizeof(Cli);
    printf ("accept()\n");
    newsocketfd = accept(socketfd, (struct sockaddr*) &Cli, &len);
    if (newsocketfd == SOCKET_ERROR)  {
        printf ("accept() failed, Err: %d \"%s\"\n",errno,strerror(errno));
        exit(1);
    }
    
    printf("connection from %s : %d\n",
           inet_ntoa(Cli.sin_addr),
           ntohs(Cli.sin_port)
           );
    /* wait for data */
    nread=0;
    printf ("read()\n");
    while( (n=read(newsocketfd, &(buf[nread]), MAXSIZE )) >0) {
        {
            printf("read %d bytes    tot=%d\n", n, n+nread);
            fflush(stdout);
            recv_len = recvfrom(newsocketfd, buf, nread, 0, (struct sockaddr *) &Cli, &len);
            printf("data: %s\n", buf);
            //Use data …
        }
        nread+=n;
    }
    if(n<=0) {
        char msgerror[1024];
        sprintf(msgerror,"read() end [result %d] ",errno);
        perror(msgerror);
        n=close(newsocketfd);
        if(n!=0)
        {
            perror("close failed: ");
            exit(2);
        }
        return(1);
    }
    
    printf ("close()\n");
    close(newsocketfd);
    close(socketfd);
    
    return(0);
}
