#include <stdio.h>
#include <unistd.h>
#include <wiringPi.h>
#include <wiringPiI2C.h>
#include <pthread.h>


typedef  unsigned char uint8_t;


void* Motorcontrol (void* param);  //thread function
void  Motorinit();
char c;


uint8_t MotorHF[7] = {7,3,0xa5,2,3,0xa5,2};
// High speed forward left + right; add explanation

uint8_t MotorST[7] = {7,0,0,0,0,0,0};
// Stop left + right; add explanation

uint8_t MotorHR[7] = {7,3,0xa5,1,3,0xa5,1};
//High speed reverse left + right; add explanation

uint8_t MotorHFR[7] = {7,3,0xa5,2,3,0xa5,1};
//High speed forward right

uint8_t MotorHFL[7] = {7,3,0xa5,1,3,0xa5,2};
//High speed forward left

int fd;


void Motorinit()
{
    uint8_t Totalpower[2]={4,230};     // power between 0 and 255
    uint8_t Softstart[3]={0x91,23,0};  // add explanation
    
    
    wiringPiSetup () ;
    pullUpDnControl(0,PUD_DOWN);
    
    fd=wiringPiI2CSetup(0x32);
    
    write(fd,&Totalpower[0], 2);
    write(fd,&Softstart[0],3);
    
    //number of bytes = 3
    //What is a soft start?
}


void forward(char c){
    write(fd,&MotorHF[0],7);  //forward
    printf("Forward\n");
    usleep(2000000);
}
void backward(char c){
    write(fd,&MotorHR[0],7);  //reverse
    printf("Reverse\n");
    usleep(2000000);
}

void right(char c){
    write(fd,&MotorHFR[0],7);//right
    printf("Right\n");
    usleep(500000);
    
}

void left(char c){
    write(fd,&MotorHFL[0],7);//left
    printf("Left\n");
    usleep(500000);
    
}

void stop(char c){
    write(fd,&MotorST[0],7);  //stop
    printf("Break\n");
    usleep(2000000);
}

void inputCheck(char c){
    printf("Give input:");
    scanf("%c", &c);
    switch(c){
        case 'w': forward(c);break;
        case 's': backward(c);break;
        case 'a': left(c);break;
        case 'd': right(c);break;
        case 'b': stop(c);break;
        default: stop(c);
    }
    
    
}

int main()
{
    pthread_t tid1;
    
    Motorinit();
    
    pthread_create(&tid1,NULL,Motorcontrol,0);
    //Create and start a new posix thread
    
    pthread_join(tid1,NULL);
    // add an explanation
    
    
    return 0;
}



// The function Motorcontrol runs in a posix thread

void* Motorcontrol(void* param)
{
    while (1)
    {
        inputCheck(c);
    }
}
